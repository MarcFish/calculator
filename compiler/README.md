1. 文件说明
    1. calculator.g4: ANTLR4的语法描述文件；内容是四则运算的基本语法和词法；
    2. main.cpp: 主文件；包含了主函数；
    3. visitor.h & visitor.cpp：ANTLR4生成的访问器方法的具体实现；
    4. Makefile：
    5. GlobalContext.h & GlobalContext.cpp: 全局上下文，包含了LLVM的一些初始化方法，生成IR要用到的module、builder，block等。
2. 使用说明
    1. make visitor  // 调用ANTLR4解析语法，生成访问器类
    2. make main  // 编译
    3. ./main ../tests/test1.cal  // 输入测试文件获得输出结果
3. 简要说明
    1. 该解释器利用ANTLR4对四则运算语法进行解析获得相应分析树以及访问器父类。通过实现分析树对应节点的访问器方法，该解释器可以实现四则运算的基本功能；
    2. main.cpp中包含了主要的流程：词法分析->语法分析->语义分析 & 获取计算结果
    3. visitor.h & visitor.cpp 中包含了访问器方法的实现，主要为对父类方法的实现以及一个字典`antlr4::tree::ParseTreeProperty<double> valueTree`。该字典存储了分析树节点以及对应的值。设计该字典是因为ANTLR4的Cpp版本访问器的返回值不支持泛型，而通过强转实现又比较繁琐。
    3. 在任一个访问器方法如`antlrcpp::Any Visitor::visitStat(calculatorParser::StatContext *context)`中，输入为分析树的相应节点，输出为空。在方法中，首先实现访问子节点以获取子节点结果，然后计算此节点应当做的动作。最后将此节点的值存入字典中，以方便之后使用。
    4. 由于LLVM IR的常数实现，该编译器最后会直接将结果预运算并放在目标代码中，而不是在目标代码中进行运算。
#include <iostream>
#include <string>

#include "antlr4-runtime.h"

#include "llvm/ADT/APFloat.h"
#include "llvm/ADT/STLExtras.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/DerivedTypes.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Type.h"
#include "llvm/IR/Verifier.h"
#include "llvm/Support/TargetSelect.h"
#include "llvm/ExecutionEngine/ExecutionEngine.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/Support/TargetRegistry.h"
#include "llvm/Support/TargetSelect.h"
#include "llvm/Target/TargetMachine.h"
#include "llvm/Target/TargetOptions.h"

#include "calculatorLexer.h"
#include "calculatorParser.h"


#include "visitor.h"
#include "GlobalContext.h"


int main(int argc, const char *argv[]){
    if (argc != 2) {
        std::cout<<"wrong input format"<<std::endl;
        return 1;
    }
    GlobalContext& gctx = GlobalContext::GetInstance();
    std::ifstream stream(argv[1], std::ios::binary);
    antlr4::ANTLRInputStream input(stream);
    calculatorLexer lexer(&input);
    antlr4::CommonTokenStream tokens(&lexer);
    tokens.fill();
    calculatorParser parser(&tokens);
    parser.setBuildParseTree(true);
    antlr4::tree::ParseTree* tree = parser.stat();
    Visitor visitor;
    visitor.visit(tree);
    gctx.emit();

    return 0;
}

#pragma once

#include <map>
#include <string>

#include "calculatorBaseVisitor.h"
#include "GlobalContext.h"

class Visitor: public calculatorBaseVisitor{
    public:
        antlrcpp::Any visitStat(calculatorParser::StatContext *context) override;
        
        antlrcpp::Any visitExpUnary(calculatorParser::ExpUnaryContext *context) override;

        antlrcpp::Any visitExpParent(calculatorParser::ExpParentContext *context) override;

        antlrcpp::Any visitExpNumber(calculatorParser::ExpNumberContext *context) override;

        antlrcpp::Any visitExpPow(calculatorParser::ExpPowContext *context) override;

        antlrcpp::Any visitExpMulDiv(calculatorParser::ExpMulDivContext *context) override;

        antlrcpp::Any visitExpAddSub(calculatorParser::ExpAddSubContext *context) override;

        antlrcpp::Any visitNumber(calculatorParser::NumberContext *context) override;
        
        antlrcpp::Any visitOpUnary(calculatorParser::OpUnaryContext *context) override;

        antlrcpp::Any visitOpMulDiv(calculatorParser::OpMulDivContext *context) override;

        antlrcpp::Any visitOpAddSub(calculatorParser::OpAddSubContext *context) override;

        void setValue(antlr4::tree::ParseTree *node, llvm::Value *value);
        llvm::Value *getValue(antlr4::tree::ParseTree *node);
    private:
        antlr4::tree::ParseTreeProperty<llvm::Value*> valueTree;
};

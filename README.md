### ANTLR4+LLVM的简单例子
1. 简介
    1. 该教程旨在描述如何利用ANTLR4+LLVM实现一个简单的计算器。calculator语法仅包含常见的四则运算，不包含程序语言如函数、分支或循环等。利用ANTLR4对四则运算语法进行解析获得语法树，然后通过LLVM转化为汇编获得运算结果。
    2. 教程分为两部分，一部分是编译器，一部分是解释器。解释器仅包含ANTLR4，没有利用到LLVM（后续会增添为LLVM JIT）。编译器则是包含了ANTLR4和LLVM。
    3. ANTLR4与LLVM的结合主要思想是在ANTLR4生成的语法树中调用LLVM IR Builder构建LLVM IR。
2. 环境：Ubuntu20.04+ANTLR4 3.9.3+LLVM 10
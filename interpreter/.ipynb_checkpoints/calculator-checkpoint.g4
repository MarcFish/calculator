grammar calculator;

stat: exp;

exp
    : number  # expNumber
    | Lparen exp Rparen  # expParent
    | <assoc=right> exp OpPow exp  # expPow
    | opUnary exp  # expUnary
    | exp opMulDiv  exp  # expMulDiv
    | exp opAddSub exp  # expAddSub
    ;

number
    : Int
    | Float
    ;

opUnary
    : Plus
    | Minus
    ;

opMulDiv
    : Mul
    | Div
    ;

opAddSub
    : Plus
    | Minus
    ;

OpPow : '^';
Lparen : '(';
Rparen : ')';
Plus: '+';
Minus: '-';
Mul: '*';
Div: '/';
Int: Digit+;
Float : Digit+ '.' Digit*;

fragment
Digit
    : [0-9]
    ;

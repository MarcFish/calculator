#include <iostream>
#include <string>

#include "antlr4-runtime.h"

#include "calculatorLexer.h"
#include "calculatorParser.h"

#include "visitor.h"

int main(int argc, const char *argv[]){
    if (argc != 2) {
        std::cout<<"wrong input format"<<std::endl;
        return 1;
    }
    std::ifstream stream(argv[1], std::ios::binary);
    antlr4::ANTLRInputStream input(stream);
    calculatorLexer lexer(&input);
    antlr4::CommonTokenStream tokens(&lexer);  // 词法分析
    tokens.fill();
    calculatorParser parser(&tokens);  // 语法分析
    parser.setBuildParseTree(true);
    antlr4::tree::ParseTree* tree = parser.stat();  // 获取分析树
    Visitor visitor;
    visitor.visit(tree);  //遍历分析树， 获取结果
    return 0;
}
